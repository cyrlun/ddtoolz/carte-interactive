class Buildings {
    constructor(known, name, width = null, deep = null, description, x, y, quartier = null, type) {
        this.known = known;
        this.name = name;
        this.width = width;
        this.deep = deep;
        this.description = description;
        this.x = x;
        this.y = y;
        this.quartier = quartier;
        this.type = type;
    }

}
//tavernes
class Taverns extends Buildings {
    constructor(known, name, width, deep, description, x, y, owner, standing, nbFloor, questBoard, nbQuests, nbRooms, type) {
        super(known, name, width, deep, description, x, y, owner, standing, nbFloor, questBoard, nbQuests, nbRooms, type)
        this.owner = owner;
        this.known = known
        this.width = width;
        this.deep = deep;
        this.description = description;
        this.x = x;
        this.y = y;
        this.known = known;
        this.owner = owner;
        this.standing = standing;
        this.nbFloor = nbFloor;
        this.questBoard = questBoard;
        this.nbQuests = nbQuests;
        this.nbRooms = nbRooms;
        this.type = type;
    }

}
class TavernBasQuartier extends Taverns {
    quartier = "Bas-Quartier"
}
class TavernAventurier extends Taverns {
    quartier = "Aventurier"
}
class TavernMarchand extends Taverns {
    quartier = "Marchand"
}
class TavernNoble extends Taverns {
    quartier = "Noble"
}
//Boutiques
class Shops extends Buildings {
    constructor(known, name, width, deep, description, x, y, owner, standing, type) {
        super(known, name, width, deep, description, x, y, owner, standing, type)
        this.type = type;
        this.owner = owner;
        this.standing = standing;
    }

}
class ShopBasQuartier extends Shops {

    quartier = "Bas-Quartier"
}
class ShopAventurier extends Shops {
    quartier = "Aventurier"
}
class ShopMarchand extends Shops {
    quartier = "Marchand"
}
class ShopNoble extends Shops {
    quartier = "Noble"
}
//liste des tavernes par quartier
const myBuildings = {
    basQuartier: {
        taverns: [
            new TavernBasQuartier(true, "La mouche dorée", 3, 6, "Non visitée", 515,250, "Grohk 'Le Pestilenciel' Akkha", "Très Basse", 1, true, 2, 1, "Taverne"),
            new TavernBasQuartier(true, "La lance émoussée", 3, 6, "Non visitée", 355, 543, "Bal-Hofs 'Les rayonnantes' Stilz", "Basse", 1, true, 2, 1, "Taverne"),
        ],
        shops: [
            new ShopBasQuartier(true, "aaaaaaaa", 3, 6, "Non visitée", 526, 180, "Illar 'L\'escroc' Hant", "Haut", "Boutique"),
        ],
        famous: [
            new Buildings(true, "Quais des Eaux-du-Bilge", 100, 50, "Des quais en bois en mauvais état comparé à la rive d'en face.", 470, 100, "Bas-quartier", "Lieu Notable"),
        ]
    },
    aventurier: {
        taverns: [
            
            new TavernAventurier(true, "La loutre enragée", 5, 8, "Une taverne majoritairement fréquentée par des gardes. <br>Après un bref passage, beaucoup de malchance et un accueil peu chaleureux, les aventuriers décident de passer leur chemin.", 375, 235, "Git 'Tartophalange' Longuejambe", "Moyen +", 2, true, 3, 6, "Taverne"),
           
            new TavernAventurier(true, "Chez Bertie", 5, 8, "Une taverne avec une histoire atypique. <br> Elle fût apparemment séparée en deux après un désaccord des deux gérants.<br> Les héros n'en savent pas plus à ce sujet.", 294, 160, "Bertie 'Bon-dos' Allichar", "Moyen +", 2, true, 3, 6, "Taverne"),
           
            new TavernAventurier(true, "& Samwell", 5, 8, "Non visitée", 284, 160, "Samwell 'Le sournois' Allichar", "Moyen +", 2, true, 3, 6, "Taverne")
        ],
        shops: [
            //(known,name,width,deep,description,x,y,owner,standing)
            new ShopAventurier(true, "aaaaaaaa", 3, 6, "Non visitée", 322, 115, "Illar 'L\'escroc' Hant", "Haut", "Boutique"),
        ],
        famous: [
            new Buildings(true, "Arène de Oaveligt", 100, 50, "Une immense arène où se déroule Le Tournoi des Guildes. <br>A visiter au moins une fois pour admirer la magie qui opère en ces lieux", 350, 195, "Aventurier", "Lieu Notable"),
        ]
    },
    marchand: {
        taverns: [
            new TavernMarchand(true, "Dalibaba", 5, 6, "Non Visitée", 570, 170, "Lakav Herne", "Haute", true, 3, 6, "Non visitée", "Taverne"), // ajuster le nombre de paramètres à 14 pour pouvoir afficher 

        ],
        shops: [
            new ShopMarchand(true, "aaaaaaaa", 3, 6, "Non visitée", 10, 20, "Illar 'L\'escroc' Hant", "Haut", "Boutique"),
        ],
        famous: [
            new Buildings(true, "Foire d'Hogma", 100, 50, "Un marché gigantesque, on y trouve de tout pour peu que l'on ait de la chance et des poches bien pleines", 30, 30, "marchand", "Lieu Notable"),
        ]
    },
    noble: {
        taverns: [
            new TavernNoble(true, "Maison des Mille fleurs", 5, 8, "Non visitée", 420, 365, " Yphria d'Esharzy", "Très haute", 2, true, 3, 6, "Taverne"),
        ],
        shops: [
            new ShopNoble(true, "La crosse d'argent", 3, 6, "Non visitée", 500, 440, "Plynn Léfouille", "Haut","Boutique"),
        ],
        famous: [
            new Buildings(true, "Tour de l'horloge", 100, 50, "Une immense tour mécanique qui fût offerte à Oaveligt par le grand ingénieur gnome Glax TourneFusée. <br>Sur la partie haute, au niveau du cadran se joue un spectacle fascinant toutes les trois heures.", 620, 710, "noble", "Lieu Notable"),
        ]
    }
}
console.log(myBuildings.basQuartier.taverns);

const knownBasQuartierTaverns = document.getElementById('known-bas-quartier-taverns')
const knownAdventureTaverns = document.getElementById('known-adventure-taverns')
const knownMarchandTaverns = document.getElementById('known-marchand-taverns')
const knownNobleTaverns = document.getElementById('known-noble-taverns')

const knownBasQuartierShop = document.getElementById('known-bas-quartier-shop')
const knownAdventureShop = document.getElementById('known-adventure-shop')
const knownMarchandShop = document.getElementById('known-marchand-shop')
const knownNobleShop = document.getElementById('known-noble-shop')

const knownBasQuartierFamous = document.getElementById('known-bas-quartier-famous')
const knownAdventureFamous = document.getElementById('known-adventure-famous')
const knownMarchandFamous = document.getElementById('known-marchand-famous')
const knownNobleFamous = document.getElementById('known-noble-famous')

const showInfoGauche = () => {
    const infoGauche = document.getElementById('info-gauche')
    infoGauche.innerHTML = ''
    infoGauche.classList.remove('d-none')
}

const showBuildings = (buildings, id) => {

    showInfoGauche()

    buildings.forEach((element, index) => {
        console.log(typeof index);
        // Conditionnement de l'affichage des tavernes si elle est connue des joueurs
        if (element.known) {
            let newDiv = document.createElement("div")
            newDiv.setAttribute('id', `empty${index+1}`)
            newDiv.setAttribute('class', 'tavern-container pt-0 mb-0')
            // positionnement dans la colonne info-gauche
            let currentDiv = document.getElementById('info-gauche');
            currentDiv.appendChild(newDiv);
            // remplissage des informations des tavernes
            let type = element.type;
            let place = document.getElementById(`empty${index+1}`)

            if ((type == "Taverne") || (type == "Boutique")) {

                place.innerHTML = `<div class="container row">
                                    <button class="tavern-description text-left" type="button" id="${id}-${index+1}">
                                        <p><strong>Nom:</strong><br>${element.name}</p>
                                        <p class='description my-0'><strong>Propriétaire:</strong><br>${element.owner}</p>
                                        <p class='description my-0'><strong>Qualité de la ${element.type}:</strong><br>${element.standing}</p>
                                    </button>
                                    <button type="button" class="btn-danger btn-custom" data-toggle="modal" data-target="#modal-${id}${index+1}">Détails connus</button>
                                        <div class="modal fade" id="modal-${id}${index+1}" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">${element.name}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                    </div>
                                                <div class="modal-body">
                                                    <p>${element.description}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
            } else if (type == "Lieu Notable") {
                place.innerHTML = `<div class="container row">
                                    <button class="tavern-description text-left" type="button" id="${id}-${index+1}">
                                        <p><strong>Nom:</strong><br>${element.name}</p>
                                    </button>
                                    <button type="button" class="btn-danger btn-custom" data-toggle="modal" data-target="#modal-${id}${index+1}">Détails connus</button>
                                        <div class="modal fade" id="modal-${id}${index+1}" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">${element.name}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                    </div>
                                                <div class="modal-body">
                                                    <p>${element.description}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
            }
            const placePings = () => {
                const ping = document.getElementById('ping')
                ping.style.top = `${element.y}px`
                ping.style.left = `${element.x}px`
                console.log(element)
            }
            const showPings = () => {
                let ping = document.getElementById('ping');
                if (ping == null) {
                    let newDiv = document.createElement('div')
                    newDiv.setAttribute('id', 'ping')
                    let icon = document.createElement('div')
                    icon.setAttribute('id', 'round')
                    newDiv.appendChild(icon)
                    let currentDiv = document.getElementById('map-relative')
                    currentDiv.appendChild(newDiv)
                } else {
                    ping.remove()
                    let newDiv = document.createElement('div')
                    newDiv.setAttribute('id', 'ping')
                    let icon = document.createElement('div')
                    icon.setAttribute('id', 'round')
                    newDiv.appendChild(icon)
                    let currentDiv = document.getElementById('map-relative')
                    currentDiv.appendChild(newDiv)
                }
                placePings()
            }
            const showPlace = document.querySelector(`button[id|="${id}-${index+1}"]`)
            showPlace.onclick = () => showPings()
        }
        let currentDiv = document.getElementById('info-gauche')
        let newDiv = document.createElement('button')
        let newModal = document.createElement('div')
        let modalExist = document.querySelector('button[data-target="#modal-ajout-batiment"')
        const firstChild = currentDiv.firstChild
        
        if (modalExist == null) {
            newDiv.classList.add('btn-danger','mt-2','rounded','bg-danger','btn__ajout')
            newDiv.setAttribute("type",'button')
            newDiv.setAttribute("data-toggle","modal")
            newDiv.setAttribute("data-target","#modal-ajout-batiment")
            newDiv.innerHTML = `Ajouter un bâtiment`
    
            currentDiv.insertBefore(newDiv,firstChild)
            currentDiv.appendChild(newModal)
            let str = id.slice(0,1)
            console.log(typeof str);
            newModal.innerHTML = 
            `<div class="modal fade" id="modal-ajout-batiment" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form>
                        <div class="modal-header">
                            <div class="row ml-2">
                                <label for="building-name" class="col-form-label">Nom du batiment:</label>
                                <input type="text" class="form-control" id="building-name">
                                <div class="form-group">
                                    <label for="building-name" class="col-form-label">Type du batiment:</label>
                                    <input type="text" class="form-control" id="building-name" value="${element.type}"
                                        readonly="true">
                                </div>
                                <div class="form-group">
                                    <label for="building-name" class="col-form-label ml-3">Quartier:</label>
                                    <input type="text" class="form-control ml-3" id="building-name" value="${str.toUpperCase()}${id.slice(1)}"
                                        readonly="true">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Propriétaire:</label>
                                    <input type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group col-12 px-0">
                                    <label for="message-text" class="col-form-label">Description:</label>
                                    <textarea class="form-control" id="message-text"></textarea>
                                </div>
    
                            </div>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">x</span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>`  
        }
        
    }
)





  
 







}
const showPlace = document.querySelector('button[id|="aventurier"]')

knownBasQuartierTaverns.onclick = () => showBuildings(myBuildings.basQuartier.taverns, "bas-quartier")
knownAdventureTaverns.onclick = () => showBuildings(myBuildings.aventurier.taverns, "aventurier")
knownMarchandTaverns.onclick = () => showBuildings(myBuildings.marchand.taverns, "marchand")
knownNobleTaverns.onclick = () => showBuildings(myBuildings.noble.taverns, "noble")

knownBasQuartierShop.onclick = () => showBuildings(myBuildings.basQuartier.shops, "bas-quartier")
knownAdventureShop.onclick = () => showBuildings(myBuildings.aventurier.shops, "aventurier")
knownMarchandShop.onclick = () => showBuildings(myBuildings.marchand.shops, "marchand")
knownNobleShop.onclick = () => showBuildings(myBuildings.noble.shops, "noble")

knownBasQuartierFamous.onclick = () => showBuildings(myBuildings.basQuartier.famous, "bas-quartier")
knownAdventureFamous.onclick = () => showBuildings(myBuildings.aventurier.famous, "aventurier")
knownMarchandFamous.onclick = () => showBuildings(myBuildings.marchand.famous, "marchand")
knownNobleFamous.onclick = () => showBuildings(myBuildings.noble.famous, "noble")