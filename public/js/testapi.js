// EXERCICE FETCH 1


function testFetch() {

    fetch("https:/mockbin.com/request?greetings=salut")
        .then(function (response) {
            return response.json()
        })
        .then(function (value) {

            document.getElementById('testapi').innerText = value.queryString.greetings
        })
        .catch(function (err) {
            console.log(err);
        })
}
document.getElementById('btn-api').addEventListener("click", testFetch)


// EXERCICE SECURISATION DE DONNEES

function getCodeValidation() {
    return document.getElementById('code-validation')
}

function disableSubmit(disabled) { len
    if (disabled) {
        document.getElementById("submit-btn").setAttribute("disabled", true)
    } else {
        document.getElementById("submit-btn").removeAttribute("disabled")
    }

}

document.getElementById('code').addEventListener('input', function (e) {
    if (/^CODE-/.test(e.target.value)) {
        getCodeValidation().innerText = "Code Valide"

    } else {
        getCodeValidation().innerText = "Code invalide"
        disableSubmit(true)
    }
})






function send(e) {
    e.preventDefault();
    let inputValue = document.getElementById("value")

    fetch("https://mockbin.com/request", {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            value: inputValue.value
        })
    }).then(function (res) {

        if (res.ok) {
            return res.json()
        }
    }).then(function (value) {

        const myNumbers = localStorage.getItem("myNumbers") !== null ? JSON.parse(localStorage.getItem("myNumbers")) : []
        console.log(myNumbers);
        const myNumber = JSON.parse(value.postData.text).value
        myNumbers.push(myNumber)
        localStorage.setItem("myNumbers", JSON.stringify(myNumbers))
        document.getElementById("result").innerHTML = `<div  style="background:red; max-width:300px;">${value.postData.text}</div>`;
        const div = document.getElementById('test')
        div.innerHTML = ''
        inputValue.value = ''
        myNumbers.forEach(element => {
            console.log(element);
            div.innerHTML += `<p>${element}</p>`
        });
    })

}
document.getElementById("form").addEventListener("submit", send)