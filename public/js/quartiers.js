const btnBQ = document.getElementById('btnBQ')
const btnA = document.getElementById('btnA')
const btnM = document.getElementById('btnM')
const btnN = document.getElementById('btnN')

const basQuartier = document.getElementById('zoomBQ')
const aventurier = document.getElementById('zoomA')
const marchand = document.getElementById('zoomM')
const noble = document.getElementById('zoomN')



const removeDisplayNone = (element,element2,element3,element4) => {
    const checkDNone = document.querySelector("img[class='map-quartier d-none'")
    const ping = document.getElementById('ping')
    if(ping !== null){
        ping.remove()
    }
    
    if (checkDNone !== null) {
        
        element.setAttribute('class','map-quartier')
        var delayInMilliseconds =300
        setTimeout(function(){
            element2.setAttribute('class','map-quartier d-none')
            element3.setAttribute('class','map-quartier d-none')
            element4.setAttribute('class','map-quartier d-none')
        },delayInMilliseconds)
    }
}

btnBQ.onclick = () => removeDisplayNone(basQuartier,aventurier,marchand,noble)
btnA.onclick = () => removeDisplayNone(aventurier,basQuartier,marchand,noble)
btnM.onclick = () => removeDisplayNone(marchand,basQuartier,aventurier,noble)
btnN.onclick = () => removeDisplayNone(noble,basQuartier,aventurier,marchand)