class Taverne {
    constructor(nom, connue, proprio, standing, nbEtages, longueur, largeur, tableauQuete, nbQuetes, nbChambres, description, x, y, quartier = null) {
        this.nom = nom;
        this.connue = connue;
        this.proprio = proprio;
        this.standing = standing;
        this.nbEtages = nbEtages;
        this.longueur = longueur;
        this.largeur = largeur;
        this.tableauQuete = tableauQuete;
        this.nbQuetes = nbQuetes;
        this.nbChambres = nbChambres;
        this.description = description;
        this.x = x;
        this.y = y;
        this.quartier = quartier;
    }
}
class TaverneBasQuartier extends Taverne {
    quartier = "Bas-Quartier"
}

class TaverneAventurier extends Taverne {
    quartier = "Aventurier"
}
class TaverneMarchand extends Taverne {
    quartier = "Marchand"
}
class TaverneNoble extends Taverne {
    quartier = "Noble"
}

const myTaverns = {
    basQuartier: [
        new TaverneMarchand("La mouche dorée", true, "Grohk 'Le Pestilenciel' Akkha", "Très Basse", 2, 20, 10, true, 3, 6, "Non visitée"),
        new TaverneBasQuartier("La lance émoussée", true, "Bal-Hofs 'Les rayonnantes' Stilz", "Basse", 2, 20, 10, true, 3, 6, "Non visitée"),

    ],
    aventurier: [
        new TaverneAventurier("La loutre enragée", true, "Git 'Tartophalange' Longuejambe", "Moyen +", 2, 20, 10, true, 3, 6, "Une taverne majoritairement fréquentée par des gardes. <br>Après un bref passage, beaucoup de malchance et un accueil peu chaleureux, les aventuriers décident de passer leur chemin."),
        new TaverneAventurier("Chez Bertie", true, "Bertie 'Bon-dos' Allichar", "Austère", 2, 20, 10, true, 3, 6, "Une taverne avec une histoire atypique. <br> Elle fût apparemment séparée en deux après un désaccord des deux gérants.<br> Les héros n'en savent pas plus à ce sujet."),
        new TaverneAventurier("& Samwell", true, "Samwell 'Le sournois' Allichar", "Austère", 2, 20, 10, true, 3, 6, "Non visitée"),
        new TaverneAventurier("La roue paresseuse", true, "Hubiya 'La nonchalante' Yillandir", "Moyen", 2, 20, 10, true, 3, 6, "Non visitée"),
        new TaverneAventurier("Le rire de Shasha", true, "Shasha 'La vertueuse' Faldir", "Très bonne", 2, 20, 10, true, 3, 6, "Non visitée"),
        new TaverneAventurier("sdufgqosuhdf", true, "qdfgdfggqdg", "???", 2, 20, 10, true, 3, 6, "Non visitée"),
    ],
    marchand: [
        new TaverneMarchand("La Table de la Wyverne", true, "Garruk 'Bras-Sanglant' Bornecendre", "Austère", 2, 20, 10, true, 3, 6, "Non visitée"),
    ],
    noble: [
        new TaverneNoble("Le Batôn Emeraude", true, "Allysnel 'Puit de lumière' Galliran", "Très haute", 2, 20, 10, true, 3, 6, "Non visitée")
    ]
}



const knownBasQuartierTaverns = document.getElementById('known-bas-quartier-taverns')
const knownAdventureTaverns = document.getElementById('known-adventure-taverns')
const knownMarchandTaverns = document.getElementById('known-marchand-taverns')
const knownNobleTaverns = document.getElementById('known-noble-taverns')
const showInfoGauche = () => {
    const infoGauche = document.getElementById('info-gauche')
    infoGauche.innerHTML = ''
    infoGauche.classList.remove('d-none')
}
const showTaverns = (elements,id) => {
    
    showInfoGauche()
    elements.forEach((element, index) => {
        // Conditionnement de l'affichage des tavernes si elle est connue des joueurs
        if (element.connue) {
            let newDiv = document.createElement("div")
            newDiv.setAttribute('id', `empty${index+1}`)
            newDiv.setAttribute('class', 'tavern-container pt-0 mb-0')
            // positionnement dans la colonne info-gauche
            let currentDiv = document.getElementById('info-gauche');
            currentDiv.appendChild(newDiv);
            // remplissage des informations des tavernes
            let place = document.getElementById(`empty${index+1}`)
            place.innerHTML = `<div class="container row" style="width:300px">
                                    <button class="tavern-description text-left" style="width:250px" type="button" id="${id}-${index+1}">
                                        <p><strong>Nom:</strong><br>${element.nom}</p>
                                        <p class='description my-0'><strong>Propriétaire:</strong><br>${element.proprio}</p>
                                        <p class='description my-0'><strong>Qualité de la taverne:</strong><br>${element.standing}</p>
                                    </button>
                                    <button type="button" class="btn-danger btn-custom" data-toggle="modal" data-target="#modal-${id}${index+1}">Détails connus</button>
                                        <div class="modal fade" id="modal-${id}${index+1}" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">${element.nom}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                                    </div>
                                                <div class="modal-body">
                                                    <p>${element.description}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;

        }
    })
}



knownBasQuartierTaverns.onclick = () => showTaverns(myTaverns.basQuartier,"bas-quartier")
knownAdventureTaverns.onclick = () => showTaverns(myTaverns.aventurier,"aventurier")
knownMarchandTaverns.onclick = () => showTaverns(myTaverns.marchand,"marchand")
knownNobleTaverns.onclick = () => showTaverns(myTaverns.noble,"noble")